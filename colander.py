import argparse
import pathlib
import sys
import os
import hashlib
import json
from binaryornot.check import is_binary

def start(files_pattern, cfg):
    files = find_files(files_pattern)
    analyze(files, cfg)

def find_files(files_pattern):
    matchedfiles = pathlib.Path('.').glob(files_pattern)
    files = []
    for matchedfile in matchedfiles:
        filepath = matchedfile.as_posix()
        if os.path.isfile(filepath) and not is_binary(filepath):
            files.append(filepath)
    return files

def analyze(filenames, cfg):
    lines = {}

    for filename in filenames:
        print("Parsing {0}".format(filename))

        with open(filename, 'r') as f:
            prev_hash = ''

            # read file line by line
            for filelineno, fileline in enumerate(f):
                if cfg.trim === True:
                	fileline = fileline.strip()

                if cfg.skipempty === False or fileline:
                    line_hash = hashlib.md5(fileline.encode('utf-8')).hexdigest()
                    # print("[{0}][{2}] {1}".format(line_hash, fileline, filename))

                    # if the line stopped repeating
                    if prev_hash != line_hash:
                        # insert the line in the dictionary
                        if line_hash not in lines:
                            lines[line_hash] = {
                                'text' : fileline,
                                'occurrences' : {}
                            }

                        # save the file occurence
                        if filename not in lines[line_hash]['occurrences']:
                            lines[line_hash]['occurrences'][filename] = []

                        # link the previous occurence to this line
                        if prev_hash:
                            lines[prev_hash]['occurrences'][filename][-1]['next'] = line_hash

                        # save line metadata
                        lines[line_hash]['occurrences'][filename].append({
                            'prev' : prev_hash,
                            'line' : filelineno,
                            'cnt' : 1
                        })
                    else:
                        # line repeated
                        lines[prev_hash]['occurrences'][file][-1]['cnt'] += 1

                    prev_hash = line_hash

    # find duplicated lines
    # for line_hash, line in lines.items():


if __name__ == "__main__":
    argv = sys.argv[1:]
    parser = argparse.ArgumentParser(description='Detects copy-pasted code')
    parser.add_mutually_exclusive_group(required=False)

    parser.add_argument('source', metavar='s', type=str, nargs='?', default='*.*',
        help='The pattern of files that need to be matched')
    parser.add_argument('--skipempty', dest='skipempty', action="store_false",
    	help='If passed, empty lines are not skipped')
    parser.add_argument('--trim', dest='trim', action="store_false",
    	help='If passed, lines are trimmed')

    parser.set_defaults(
    	skipempty=False,
    	trim=False
    	)
    args = parser.parse_args()

    start(args.source, args)
